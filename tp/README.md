# CESI DevOps - TP

Avant de commencer, il vous faudra configurer votre poste afin de réaliser les TPs. Direction [la section préparation de poste](../cours/prep_poste.md) pour plus d'infos.

L'OS GNU/Linux recommandé est CentOS 7.

TPs :
* [IaC Intro](./iac-intro)
