# Préparation du poste de travail

Il vous faudra :
* un hyperviseur : créer et gérer des VMs
* un client SSH : se connecter et administrer les VMs
* Vagrant : outil d'automatisation de gestion de VMs
* Git : outil de versionning utilisé pour héberger et gérer du code

<!-- vim-markdown-toc GitLab -->

* [Installation d'un hyperviseur](#installation-dun-hyperviseur)
* [Client SSH](#client-ssh)
    * [GNU/Linux et MacOS](#gnulinux-et-macos)
    * [Windows](#windows)
* [Vagrant](#vagrant)
* [Git](#git)

<!-- vim-markdown-toc -->

# Installation d'un hyperviseur

Je vous recommande d'utiliser [VirtualBox](https://www.virtualbox.org/) pendant nos cours.

Vous pouvez utiliser un autre hyperviseur, mais j'attendrai de vous que vous le maîtrisiez (je vous apporterai moins de support qu'avec Vitualbox).

# Client SSH

Il vous faudra un client SSH. 

## GNU/Linux et MacOS

Vous avez un client natif en ligne de commande : la commande `ssh`

Vous pouvez générer une paire de clés à l'aide d'une commande `ssh-keygen`. Par exemple :

```bash
$ ssh-keygen -t rsa -b 4096
```

## Windows

Sur les versions récentes de Windows 10, vous avez aussi un client SSH natif en ligne de commande (commande `ssh` dans un powershell). Sinon, il est possible d'utiliser un outil tiers comme [Putty](https://putty.org/).

Pour générer une paire de clé SSH, il est possible que vous ayez aussi la commande `ssh-keygen` dans Powershell. Sinon, vous pouvez utiliser [Puttygen](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) (téléchargeable depuis le site de Putty).

# Vagrant

Vagrant est un outil permettant d'automatiser la création de machines virtuelles, à des fins de test (orienté plutôt pour une utilisaton laptop/lab/POC, que pour de la production).

Il met en place de façon très directe les concepts d'infrastructure as code.

Téléchargez Vagrant [depuis le site officiel](https://www.vagrantup.com/).

# Git

Enfin, il vous faudra un client Git, afin de pouvoir créer et interagir avec des dépôts Git.

Pour ce faire, on utilisera [Gitlab](https://gitlab.com/). Ce site permet d'héberger gratuitement des dépôts Git, avec plusieurs fonctionnalités avancées.
* créez vous un compte Gitlab
* testez la création du compte en créant un dépôt
* attribuez correctement les droits pour que tous les membres de votre groupe (pour les travaux à plusieurs) puissent utiliser un même dépôt

Afin de récupérer le dépôt Git et le manipuler sur votre PC :
* utiliser le client `git` en ligne de commande (dispo sur tous les OS)
* OU utiliser une interface graphique comme GitKraken

> Personnellement, je n'utilise que la ligne de commande, c'est beaucoup plus rapide et puissant. En revanche, il peut être bon de tester une application graphique afin que vous compreniez mieux ce qu'il se passe dans Git.

---

**Vous voilà fin prêts avec votre petit kit de tools DevOps. :)**
